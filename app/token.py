import os
import secrets
from datetime import datetime, timedelta
from sqlite3 import DatabaseError

from dotenv import load_dotenv
from fastapi import HTTPException

from .db import Admin, Employee, SessionLocal

# Получаем время действия токена
dotenv_path = os.path.join(os.path.dirname(__file__), '..', '.env')
load_dotenv(dotenv_path)
TOKEN_TIME = int(os.getenv('TOKEN_TIME'))


def check_token(table, token_request):
    if table not in ('Admin', 'Employee'):
        raise HTTPException(status_code=404, detail='Wrong name of table')
    try:
        with SessionLocal() as session:
            # выбираем запись по токену

            if table == 'Admin':
                token = session.query(Admin).filter_by(token=token_request).first()
            elif table == 'Employee':
                token = session.query(Employee).filter_by(token=token_request).first()

            # Проверяем токен в базе
            if not token:
                raise HTTPException(status_code=401, detail='Invalid token')

            # Проверяем срок действия токена
            if token.token_validity_period < datetime.now():

                # Проверяем к какой таблице был запрос. Если к Admin, то удаляем всю запись, к Employee - удаляем токен
                if table == 'Admin':
                    session.delete(token)
                if table == 'Employee':
                    token.token = None
                    token.token_validity_period = None
                # Сохранение изменений
                session.commit()
                raise HTTPException(status_code=401,
                                    detail='The validation period for your token has expired. Please get a new token')

    except DatabaseError as e:
        raise HTTPException(status_code=500, detail=f'Database error occurred: {str(e)}')
    except HTTPException as e:
        raise HTTPException(status_code=401, detail=f'Invalid token: {str(e)}')
    except Exception as e:
        raise HTTPException(status_code=500, detail=f'{str(e)}')


# Генерация случайного токена
def generate_token():
    expiration_time = datetime.now() + timedelta(minutes=TOKEN_TIME)
    token = {
        "token": secrets.token_urlsafe(32),
        "expiration_time": expiration_time.strftime("%Y-%m-%d %H:%M:%S")
    }
    return token
