import hashlib
import secrets

from fastapi import HTTPException


def hash_password(password):

    # Генерация соли
    salt = secrets.token_hex(8)

    # Конкатенация пароля и соли
    salted_password = password.encode('utf-8') + salt.encode('utf-8')

    # Хеширование пароля с солью с использованием SHA-256
    hashed_password = hashlib.sha256(salted_password).hexdigest()

    # Возвращение зашифрованного пароля и соли в виде строки
    return hashed_password, salt


def verify_password(user_hash_password, password, salt):

    # Конкатенация пароля и соли
    salted_password = password.encode('utf-8') + salt.encode('utf-8')

    # Хеширование пароля с солью с использованием SHA-256
    hashed_password = hashlib.sha256(salted_password).hexdigest()
    if hashed_password != user_hash_password:
        raise HTTPException(status_code=404, detail='Wrong password')
