import logging
import os

from dotenv import load_dotenv
from fastapi import Depends, FastAPI, HTTPException, Response
from sqlalchemy.exc import DatabaseError

from .crypt import hash_password, verify_password
from .db import Admin, Employee, EmployeeSalt, SessionLocal
from .first_start import create_table
from .token import check_token, generate_token
from .validate import (CreateToken, EmployeeCreate, EmployeeDelete,
                       EmployeePut, Token, get_create_token,
                       get_employee_create, get_employee_delete,
                       get_employee_put, get_token)

# Настраиваем логгирование
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger_handler = logging.FileHandler(f'{__name__}.log', mode='w')
logger_formatter = logging.Formatter('%(name)s %(asctime)s %(levelname)s %(message)s')
logger_handler.setFormatter(logger_formatter)
logger.addHandler(logger_handler)

# Получаем логин и пароль администратора
dotenv_path = os.path.join(os.path.dirname(__file__), '..', '.env')
load_dotenv(dotenv_path)
ADMIN_LOGIN = os.getenv('ADMIN_LOGIN')
ADMIN_PASSWORD = os.getenv('ADMIN_PASSWORD')

app = FastAPI()


# Получаем токен админа
@app.post('/admin_token')
async def admin_token(admin: CreateToken = Depends(get_create_token)):

    if admin.login == ADMIN_LOGIN and admin.password == ADMIN_PASSWORD:
        # генерируем токен для админа
        token = generate_token()
        logger.info('Create token for admin')
        try:
            with SessionLocal() as session:
                admin = Admin(
                    token=token["token"],
                    token_validity_period=token["expiration_time"]
                )
                session.add(admin)
                session.commit()
                logger.info('Save token for admin')

                response = Response()
                response.headers['X-Token'] = token["token"]
                response.headers['X-expiration_time'] = token["expiration_time"]

                return response

        except Exception as e:
            logger.exception(f'Error create token for admin: {str(e)}')
            raise HTTPException(status_code=500, detail=str(e))
    else:
        logger.exception('Wrong login, password for admin')
        raise HTTPException(status_code=400, detail='Check your login and password')


# Добавление записи в таблицу Employee
@app.post('/employee')
async def employee_post(admin_request: EmployeeCreate = Depends(get_employee_create)):

    with SessionLocal() as session:
        try:
            # Проверяем токен админа
            check_token('Admin', admin_request.token)

            # Проверяем уникальность имени пользователя
            user = session.query(Employee).filter_by(username=admin_request.username).first()
        except Exception as e:
            logger.exception(f'{str(e)}')
            raise HTTPException(status_code=500, detail=f'{str(e)}')

        if user:
            logger.exception('Try to add exists user')
            raise HTTPException(status_code=406, detail=f'User {admin_request.username} already exists')

        # получаем шифрованный пароль и соль
        password, salt = hash_password(admin_request.password)

        try:
            employee_salt = EmployeeSalt(salt=salt)
            new_employee = Employee(
                username=admin_request.username,
                password_hash=password,
                password_salt=employee_salt,
                salary=admin_request.salary,
                promotion_date=admin_request.promotion_date,
                token=None,
                token_validity_period=None
            )
            session.add(employee_salt)
            session.add(new_employee)
            session.commit()
            logger.info('Save user')

        except Exception as e:
            logger.exception(f'{str(e)}')
            raise HTTPException(status_code=500, detail=str(e))

    return {"result": f'New employee {admin_request.username} added'}


# Чтение последних 50-и записей из таблицы пользователей
@app.get('/employee')
async def employee_get(token: Token = Depends(get_token)):

    with SessionLocal() as session:
        try:
            # Проверяем токен админа
            check_token('Admin', token.token)
            # чтение 50 последних записей
            employees = session.query(Employee.username, Employee.token_validity_period). \
                order_by(Employee.id.desc()).limit(50).all()
        except Exception as e:
            logger.exception(f'{str(e)}')
            raise HTTPException(status_code=500, detail=f'{str(e)}')

        response = [{"username": employee.username,
                     "token_validity_period": employee.token_validity_period
                     } for employee in employees]
        return response


# Обновление записи в таблице Employee
@app.patch('/employee')
async def employee_patch(admin_put: EmployeePut = Depends(get_employee_put)):

    with SessionLocal() as session:
        try:
            # Проверяем токен админа
            check_token('Admin', admin_put.token)
            # Удаляем запись по username
            employee = session.query(Employee).filter_by(username=admin_put.username).first()
            if employee:
                # Если новый пароль
                if admin_put.password is not None:
                    # получаем шифрованный пароль и соль
                    password, salt = hash_password(admin_put.password)
                    employee.password_salt.salt = salt
                    employee.password_hash = password
                # Если новая зарплата
                if admin_put.salary is not None:
                    employee.salary = admin_put.salary
                # Если новая дата повышения зарплаты
                if admin_put.promotion_date:
                    employee.promotion_date = admin_put.promotion_date
                # Если нужно удалить токен
                if admin_put.token_delete is True:
                    employee.token = None
                    employee.token_validity_period = None

                session.commit()

                employee = session.query(Employee).filter_by(username=admin_put.username).first()
                response = {employee.username: "Updated"}
                return response
            else:
                raise ValueError(f'{admin_put.username} not found')
        except Exception as e:
            logger.exception(f'{str(e)}')
            raise HTTPException(status_code=500, detail=f'{str(e)}')


# Удаление из таблицы Employee
@app.delete('/employee')
async def employee_delete(admin_delete: EmployeeDelete = Depends(get_employee_delete)):

    with SessionLocal() as session:
        try:
            # Проверяем токен админа
            check_token('Admin', admin_delete.token)
            # Удаляем запись по username
            employee = session.query(Employee).filter_by(username=admin_delete.username).first()
            if employee:
                session.delete(employee)
                session.commit()
            else:
                raise ValueError(f'{admin_delete.username} not found')
        except Exception as e:
            logger.exception(f'{str(e)}')
            raise HTTPException(status_code=500, detail=f'{str(e)}')

        response = {admin_delete.username: "Delete"}
        return response


# Выдаем токен пользователю
@app.post('/employee_token')
async def employee_token(user_request: CreateToken = Depends(get_create_token)):

    try:
        with SessionLocal() as session:

            # Выбираем пользователя
            employee = session.query(Employee).filter_by(username=user_request.login).first()

            # Проверяем пользователя
            if not employee:
                logger.exception(f'User: {user_request.login} not found')
                raise HTTPException(status_code=404, detail='User not found')
            logger.info(f'Request to create token for user: {user_request.login}')

            # проверяем пароль
            employee_hash = employee.password_hash
            employee_salt = employee.password_salt.salt
            verify_password(employee_hash, user_request.password, employee_salt)
            logger.info('User verified')

            # Генерируем токен и сохраняем
            token = generate_token()
            employee.token = token["token"]
            employee.token_validity_period = token["expiration_time"]
            logger.info('Create token')

            session.commit()
            logger.info(f'Save token for {user_request.login}')

            response = Response()
            response.headers['X-Token'] = token["token"]

            return response

    except DatabaseError as e:
        logger.exception(f'Database error occurred: {str(e)}')
        raise HTTPException(status_code=500, detail=f'Database error occurred: {str(e)}')
    except HTTPException as e:
        logger.exception(f'{str(e)}')
        raise e
    except Exception as e:
        logger.exception(f'{str(e)}')
        raise HTTPException(status_code=500, detail=str(e))


# Проверка зарплаты
@app.post('/salary')
async def salary(user_token: Token = Depends(get_token)):

    try:
        with SessionLocal() as session:
            # Проверяем токен
            check_token('Employee', user_token.token)

            # Выбираем пользователя
            user = session.query(Employee).filter_by(token=user_token.token).first()

            response = {'Salary': user.salary, 'Promotion_date': user.promotion_date}

            return response

    except DatabaseError as e:
        logger.exception(f'Database error occurred: {str(e)}')
        raise HTTPException(status_code=500, detail=f'Database error occurred: {e}')
    except HTTPException as e:
        logger.exception(f'{str(e)}')
        raise e
    except Exception as e:
        logger.exception(f'{str(e)}')
        raise HTTPException(status_code=500, detail=str(e))


# создаем таблицы при запуске приложения
@app.on_event('startup')
async def startup():
    logger.info('Start app')
    create_table()
    logger.info('Create table')


# Закрываем все открытые сессии при выключении приложения
@app.on_event('shutdown')
async def shutdown():
    SessionLocal.close_all()
    logger.info('Close app')
    logger_handler.close()
