import sqlalchemy
from sqlalchemy import (DECIMAL, Column, Date, DateTime, ForeignKey, Integer,
                        String)
from sqlalchemy.orm import (backref, declarative_base, relationship,
                            sessionmaker)

from .config import settings

Base = declarative_base()
metadata = sqlalchemy.MetaData()


class Employee(Base):
    __tablename__ = "employees"
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, nullable=False)
    password_hash = Column(String(64), nullable=False)
    salary = Column(DECIMAL(10, 2))
    promotion_date = Column(Date)
    token = Column(String, nullable=True)
    token_validity_period = Column(DateTime, nullable=True)

    password_salt_id = Column(Integer, ForeignKey('password_salts.id'))
    password_salt = relationship("EmployeeSalt", backref=backref('employees', uselist=False))


class EmployeeSalt(Base):
    __tablename__ = 'password_salts'
    id = Column(Integer, primary_key=True)
    salt = Column(String(64), nullable=False)


class Admin(Base):
    __tablename__ = "admin"
    id = Column(Integer, primary_key=True)
    token = Column(String, nullable=True)
    token_validity_period = Column(DateTime, nullable=True)


engine = sqlalchemy.create_engine(settings.db_url)
metadata.create_all(engine)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
