import re
from decimal import Decimal
from typing import Optional

from fastapi import Header, HTTPException
from pydantic import BaseModel, Field
from pydantic.types import date


# Проверка типов входящих данных + получаем через Header
class CreateToken(BaseModel):
    login: str
    password: str


class Token(BaseModel):
    token: str


class EmployeeCreate(BaseModel):
    token: str
    username: str
    password: str
    salary: Decimal = Field(..., gt=Decimal("-999999999.99"), lt=Decimal("999999999.99"))
    promotion_date: date


class EmployeePut(BaseModel):
    token: str
    username: str
    password: Optional[str] = None
    salary: Optional[Decimal] = Field(None, gt=Decimal("-999999999.99"), lt=Decimal("999999999.99"))
    promotion_date: Optional[date] = None
    token_delete: Optional[bool] = None


class EmployeeDelete(BaseModel):
    token: str
    username: str


def get_employee_create(
        token: str = Header(..., alias='X-Admin_Token'),
        username: str = Header(..., alias='X-username'),
        password: str = Header(..., alias='X-password'),
        salary: Decimal = Header(..., alias='X-Salary'),
        promotion_date: date = Header(..., alias='X-Promotion-Date.YYYY-MM-DD')
):
    validate_token(token)
    validate_user(username)
    validate_password(password)

    return EmployeeCreate(
        token=token,
        username=username,
        password=password,
        salary=salary,
        promotion_date=promotion_date
    )


def get_employee_put(
    token: str = Header(..., alias='X-Admin_Token'),
    username: str = Header(..., alias='X-username'),
    password: Optional[str] = Header(None, alias='X-password'),
    salary: Optional[Decimal] = Header(None, alias='X-Salary'),
    promotion_date: Optional[date] = Header(None, alias='X-Promotion-Date.YYYY-MM-DD'),
    token_delete: Optional[bool] = Header(None, alias='X-Token-Delete')
):
    validate_token(token)
    validate_user(username)
    if password:
        validate_password(password)

    return EmployeePut(
        token=token,
        username=username,
        password=password,
        salary=salary,
        promotion_date=promotion_date,
        token_delete=token_delete
    )


def get_create_token(
        login: str = Header(..., alias='X-Login'),
        password: str = Header(..., alias='X-Password')
):
    validate_user(login)
    validate_password(password)
    return CreateToken(login=login, password=password)


def get_token(
        token: str = Header(..., alias='X-Token')
):
    validate_token(token)
    return Token(token=token)


def get_employee_delete(
    token: str = Header(..., alias='X-Token'),
    username: str = Header(..., alias='X-Username')
):
    validate_token(token)
    validate_user(username)
    return EmployeeDelete(token=token, username=username)


# Валидация логина
def validate_user(user):
    if len(user) < 5:
        raise HTTPException(status_code=401, detail='Minimum length username 4 symbol')

    pattern = r'^[A-Za-z0-9_.\-]+$'
    if not re.match(pattern, user):
        raise HTTPException(status_code=401, detail='Username can contain only: a-z A-Z 0-9 _ . -')


# Валидация пароля
def validate_password(pas):
    if len(pas) < 8:
        raise HTTPException(status_code=401, detail='Minimum length password 8 symbol')

    pattern = r'^[A-Za-z0-9_.\-!@#$%^&*]+$'
    if not re.match(pattern, pas):
        raise HTTPException(status_code=401, detail='Password can contain only: a-z A-Z 0-9 _ . - ! @ # $ % ^ & *')


# Валидация токена
def validate_token(token):
    pattern = r'^[A-Za-z0-9_\-]+$'
    if not re.match(pattern, token):
        raise HTTPException(status_code=401, detail='Token contain unacceptable characters')
