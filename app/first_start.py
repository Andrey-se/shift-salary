from sqlalchemy import create_engine, inspect

from .config import settings
from .db import Admin, Employee


# Создание таблиц
def create_table():
    engine = create_engine(settings.db_url)
    inspector = inspect(engine)
    existing_tables = inspector.get_table_names()

    # Если таблиц нет, то создаем
    if Admin.__tablename__ not in existing_tables:
        Admin.metadata.create_all(bind=engine)

    if Employee.__tablename__ not in existing_tables:
        Employee.metadata.create_all(bind=engine)
